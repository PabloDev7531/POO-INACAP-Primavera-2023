#Creando clase Usuario
class Usuario:
    
    #Metodo constructor
    def __init__(self, nombre):
        self.nombre = nombre
        self.tareas = [] #Relación uno a muchos con Tareas
    
    #Creando metodo para agregar tareas
    def agregar_tareas(self, tarea):
        self.tareas.append(tarea)
    
    #Creando metodo para listar tareas    
    def listar_tareas(self):
        print(f"Tareas de {self.nombre}: ")
        #Recorriendo lista y mostrando cada uno de los elementos que contiene
        for tarea in self.tareas:
            print(tarea)    
        print(" ")    
        