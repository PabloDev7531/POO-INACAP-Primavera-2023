from Usuario import Usuario
from Tarea import Tarea

#Creando usuarios
usuario1 = Usuario("Pablo")
usuario2 = Usuario("Catalina")
usuario3 = Usuario("Kurumi")

#Creando tareas
tarea1 = Tarea("Estudiar Front")
tarea2 = Tarea("Estudiar Back")
tarea3 = Tarea("Estudiar BD")

#Agregando tareas a usuarios
usuario1.agregar_tareas(tarea1)
usuario2.agregar_tareas(tarea2)
usuario3.agregar_tareas(tarea3)

#Mostrar las tareas de los usuarios
usuario1.listar_tareas()
usuario2.listar_tareas()
usuario3.listar_tareas()